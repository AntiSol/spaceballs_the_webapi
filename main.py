"""

SPACEBALLS: The Web API

By Dale Magee, (C) 2021

BSD 3-clause license.


sudo apt-get install build-essential python3-psycopg2 python-psycopg2-doc

"""

import sys,os,re,random #some usually-useful things

from fastapi import FastAPI, Response, Header
from fastapi.responses import PlainTextResponse, HTMLResponse
from fastapi.middleware.cors import CORSMiddleware

from typing import List, Optional

from pydantic import BaseModel

#I just went for one big experimental file rather than splitting it out.

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship


#load config for the the spaceballs api:
from config import *

#SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
SQLALCHEMY_DATABASE_URL = SPACEBALLS_DBSTRING

#is print harmless? seems to just log?
print("venv: "+sys.prefix)

#database
engine = create_engine(
	SQLALCHEMY_DATABASE_URL, #connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

app = FastAPI()

origins = [
	"https://antisol.org",
	"http://antisol.org",
	"http://localhost",
	"http://localhost:8000",
]

app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"],
)


class DBQuote(Base):
	"""
	Adaptor from Quote to postgres
	"""
	__tablename__ = "quotes"

	id = Column(Integer, primary_key=True, index=True)
	name = Column(String, primary_key=True,index=True)
	
	start_line= Column(Integer,index=True,default=0)
	num_lines= Column(Integer,index=True,default=1)
	
	description = Column(String, index=True)
	
	
	
	#owner_id = Column(Integer, ForeignKey("users.id"))
	#owner = relationship("User", back_populates="items")
	
class Quote(BaseModel):
	"""
	This is a spaceballs quote.
	
	A Quote has a name once it has been specified by the user, or saved.
	 (it's optional to specify but auto-generated)
	 The name must be unique. it is alphanumeric,
	 and allows a subset of symbols similar to a class name. 'myquote', 'quote_64', and '-404' are
	 all valid, as long as they are unique.
	
	A Quote Must have a start line >= 0 and a number of lines >= 1 <= (/lc - start)
	
	A quote may have a description. It is considered 'anonymous' and not listed
		if it does not have one.
		
	A quote has a float funniness, *usually* >-10 and <10, defaulting at 0.0, and a number
		of votes starting at zero.
	
	if a Quote is marked as predefined it gets both +100 and +10% boost to funniness in stats,
	
	"""
	name: str = None #a unique id, auto-populated on save.
	
	start_line: int
	num_lines: int
	
	description: str = None	#unlisted if no description
	
	funniness: float = 0.0
	votes: int = 0
	
	predefined: bool = False
	
	class Config:
		orm_mode = True
	


class LineElement(BaseModel):
	"""
	This is an element of a parsed line. It will have a type, text, and 
	 optionally data. a parsed line is a list of LineElements
	This is a cheap way to htmlify our output.
	Data can be used for things like character names
	"""
	type: str
	data: str = ""
	text: str
	

datafile = "spaceballs.txt"

"""
A crude memory cache to prevent us from reading the file more than once
TODO: does this play nice with async?
"""
memcache = None

def read_file():
	"""
	Read and return the entire script as a list of lines
	"""
	global memcache
	if memcache == None:
		with open(datafile) as file:
			memcache = file.read().split("\n")
	return memcache

def to_text(lines=None):
	"""
	convert lines to text.
	if nothing is provided, returns the entire script.
	"""
	if lines == None: lines = read_file()
	return "\n".join(lines)

@app.get("/", response_class=PlainTextResponse)
async def get_awesomeness():
	"""
	Output the entire script of the greatest movie ever made, as text.
	"""
	return to_text(read_file())

@app.get("/download", response_class=PlainTextResponse)
async def download(response: Response):
	"""
	Download the script as a file
	"""
	#this is a nice easy case where we can simply hard-code the filename in the header ;)
	response.headers['Content-Disposition'] = "attachment;filename=Spaceballs.txt"
	return await get_awesomeness()


@app.get("/lc")
async def get_linecount():
	"""
	Returns the number of lines in the script
	"""
	return len(read_file())

@app.get("/wc")
async def get_wordcount():
	"""
	Returns the number of words in the script
	"""
	return len(re.split(r"\s",get_text()))
	
@app.get("/lines")
async def get_lines(from_line:int=0,to_line:int=None,num:int=None):
	"""
	Retrieve lines from the film script.
	note: num will override to_line if you specify both
	"""
	if to_line == None:
		to_line = await get_linecount()
	
	if num != None:
		to_line = from_line + num
	
	return read_file()[from_line:to_line]

@app.get("/rand")
async def random_lines(response: Response,
		num:int=10, 
		format:str=None, 
		accept: Optional[str] = Header(None),
):
	"""
	get randomised lines from the script
	
	num is the number of lines you want
	
	format is the format you want it in
	
	allowed formats:
		text
		html
		json
	
	you can also specify an accept header as the appropriate mime type 
	 for text/json/html
	
	"""
	start = random.randrange(await get_linecount())
	
	lines = await get_lines()
	
	while lines[start]=="": 
		start+=1
	
	lines = lines[start:start+num]
	
	#an accept header will override the format param.
	# this might not be desired.
	if not format and accept=="application/json":
		format="json"
	elif not format and accept=="text/html":
		format="html"
	elif not format and accept=="text/plain":
		format="text"
	
	
	#format and return output
	format = format.lower()
	if format == "json":
		return lines
		
	if format == "html":
		return HTMLResponse(content=to_html(parse_text("\n".join(lines))), status_code=200)
	
	#if we get here, format is unknown. default to plain text.
	return PlainTextResponse("\n".join(lines))
	
	
def parse_text(text:str=None):
	"""
	Parses the provided text (or the whole script if nothing provided)
	 using regex (not ideal, but workable because the script is static).
	 this returns a list of LineElements 
	"""
	ret = []
	
	if text != None:
		lines = text.split("\n")
	else: lines = read_file()
	
	if len(lines)>1:
		"""
		multiple lines, recurse
		"""
		for line in lines:
			parsed=parse_text(line)
			if parsed:
				ret.extend(parsed) #recurse and add to ret
		return ret
		
	if len(lines) == 1:
		"""
		one line of text, parse with regex:
		
		Note: from here on, this method is intended to have only one 
		 exit point - the final 'return ret'. 
		 
		 you probably shouldn't be using 'return' after this point unless you
		 know what you're doing.
		"""
		line=lines[0]
		
		#you need to set this to true if you do anything with ret,
		# otherwise line will be handled as a normal text node.
		match=False
		
		####
		# matches for specific lines in the film:
		
		if line == "SPACEBALLS": #first line, movie title
			ret.append(LineElement(type="title",text=line))
			match=True
			
		if line == "THE END": #last line
			ret.append(LineElement(type="endtitle",text=line))
			match=True
			
		if line == "If you can read this, you don't need glasses.":
			ret.append(LineElement(type="glasses",text=line))
			match=True
		
		
		####
		# regex matching
		
		
		#lines denoting new scenes are all uppercase.
		scene_re = r"^[^a-z\"]+$"
		if not match and re.match(scene_re,line):
			#TODO(?): we could make this more complicated by looking for things
			# like r"^(INT|EXT)"
			ret.append(LineElement(type="scene",text = line))
			match=True
		
		
		#speech starts with a character's name in all caps, then a colon, then
		#	some text. We can use regex groups to capture this easily
		speech_re = r"^([^a-z\"\:]+)\:(.*)$"
		if not match: 
			match = re.match(speech_re,line)
			if match:
				
				who, what = match.groups()
				who = who.strip()
				what = what.strip()
				html = "<span class='speech who %s'>%s:&nbsp;</span><span class='speechwhat'>%s</span>" % (who,who,what)
				ret.append(LineElement(type="speech",text=html,data=who))
		
		
		
		####
		# End of parsing, finish up
		
		if not match and line != "": #ignore empty lines
			#fallback to normal text if there was no match
			ret.append(LineElement(type="text",text=line))
			
		
	return ret
	
def to_html(parsed):
	"""
	Takes a list of parsed LineElements and outputs HTML.
	Crude method, we should probably use a HTML library of some kind (what?), 
	 but this will work for spaceballs.
	@return str
	TODO: possible to specify return type for a method like this?
	"""
	ret = ""
	
	for itm in parsed:
		classlist = "text" 
		if itm.type != classlist:
			classlist += " " + itm.type
		ret += "<p class='%s'>%s</p>" % (classlist,itm.text)
	
	return ret
	

@app.get("/quote") #,response_class=HTMLResponse)
async def get_quote(id:int=None,name:str=None,start:int=0,lines:int=10):
	"""
	retrieve a quote object
	"""
	return Quote(id=id,name=name,start_line=start,num_lines=lines)
	
	
	
	
@app.get("/html",response_class=HTMLResponse)
async def htmlify():
	"""
	Parse the text and convert it into HTML output
	TODO: add a format parameter to everything, so that we can spit out HTML
	 or text for every api method. (how?)
	"""
	return to_html(parse_text())
	

#TODO: figure out how to create paths programatically from a database.
@app.get("/intro")
async def intro():
	return await get_lines(from_line=0,num=17)
	
	
