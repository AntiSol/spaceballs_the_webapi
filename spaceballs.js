/*

SPACEBALLS: The Web API: The Front-end: The Javascript

A Dale Magee Production Featuring few-lines-of-code technologies ;)

A-FEWLINES - Antisol's Front End Web Linker/Interactor for Naturalistically Embedding Stuff

* give a link an 'apilink' class, and a data-link attribute pointing to an api endpoint, 
	(e.g '/docs'), and the link will have a href pointing at the endpoint automagically 
	added.
	
	e.g:
	<a class="apilink" data-link="/docs" >Open the Dox in a new tab</a>
 
  - by default these links will open in a new window/tab. add 'samewin' to the class 
  	to keep it in the same tab
	
	e.g: 
	<a class="apilink samewin" data-link="/endpoint" >Open an endpoint in this tab</a>
  
* give a button an apilink class and a data-link attribute, and a click event handler 
	will be set up to populate the output area with the result of the api call.
	
  - optionally, give your button an id, and then set data-param on another control 
  	 to that id, and A-FEWLINES will use that control as a parameter 
	 for the API call when the button is pressed.
	 currently supported control types: input, select
	 
	 e.g:
	 <button class="apilink" id="random" data-link="/rand">
	 <input type="text" name="num_lines" value="10" data-param="random" />
	 
	 to create a button to do an api call to /rand?num_lines=10


By Dale Magee, 2021

made with vim and scite

*/

$spaceballs = {
	
	//our API endpoint. Yes, this is ugly! CI/CD please!
	endpoint: "https://spaceballs.antisol.org:8000",
	
	debug_mode: false,
	
	
	debug: function(msg) {
		if ($spaceballs.debug_mode) console.log(msg);
	},
	
	get_loader: function() {
		return document.querySelector("div.output svg");
	},
	loader: null,	//holds our spinner
	
	init: function() {
		$spaceballs.debug("Init!");
		
		$spaceballs.loader = $spaceballs.get_loader();
		
		//this is our output element, where we put stuff
		output=document.querySelector("div.output-content");
		
		document.querySelectorAll('a.apilink').forEach(function(a){
			/**
			 * any <a> with a 'apilink' class automagically gets pointed at the api endpoint:'
			 * these elements must have a 'data-link' attribute with the api path, e.g '/foo'
			 */
			if ('data-link' in a.attributes) {
				url = $spaceballs.endpoint + a.attributes['data-link'].value;
				a.href=url;
				//by default, api links open in a new tab.
				// they can specify 'samewin' in their class list to open
				//	in the same tab
				if (!a.classList.contains("samewin"))
					a.target="_blank"; //new tab
			}
			
		});
		
		document.querySelectorAll('button.apilink').forEach(function(button){
			/**
			 * button.apilink get event listeners.
			 * these also need a data-link attribute like "/endpoint"
			 */
			if ('data-link' in button.attributes) {
				button.addEventListener('click',function(evt){
					//button click
					var querystring = "";
					
					if (button.id) {
						//find elements with a data-param set to the id of button
						document.querySelectorAll("input[data-param],select[data-param]").forEach(function(param){
							if (param.attributes['data-param'].value == button.id) {
								$spaceballs.debug("using param" + param + "for button" + button);
								
								//control matches the button, add a parameter
								if (!querystring) querystring = "?";
								else querystring += "&";
								querystring += param.name + "=" + param.value
								
							}
						});
					}
					
					//do the request:
					$spaceballs.get(button.attributes['data-link'].value + querystring);
				});
				
			}
			
			//e.addEventListener('click',function(evt){
			
		});
		
		
		
		document.querySelectorAll("input[data-param]").forEach(function(input){
			/**
			 * for inputs with a data-param attrubute, we set up a keypress handler
			 * to press the appropriate button when enter is pressed
			 */
			var id = input.attributes['data-param'].value;
			//find the button:
			button = document.querySelector("#"+id);
			if (button) {
				//set up event listener:
				input.addEventListener('keyup',function(evt){
					//$spaceballs.debug(evt);
					if (evt.keyCode==13) {
						button.click();
					}
				});
			}
			
		});
		
		//todo: select.onchange?
		
		//on document load, we load the script html into output with ajax:
		$spaceballs.get("/html");
		
	},
	
	get: function(query,headers) {
		/**
		 * do an ajax get request to the API, and populate output
		 * query should be the api query without the endpoint, e.g "/endpoint?param=value"
		 */
		
		$spaceballs.debug("$spaceballs.get('"+query+"');");
		if (!$spaceballs.loader.classList.contains('visible')) {
			$spaceballs.loader.classList.add('visible');	
		}
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {
				try { //is it JSON?
					j = JSON.parse(this.response);
					//TODO: this isn't ideal. it's better practice to jump out of
					// the try immediately here, and/or specify an exception class
					// to avoid other exceptions triggering the catch.
					html = "<h3>Got JSON Response:</h3><p>";
					html += this.response;
					html += "</p>";
					output.innerHTML = html;
					//todo: things with j
					
				} catch {
					//not JSON, treat as HTML:
					output.innerHTML = this.response;
				}
				
				$spaceballs.loader.classList.remove('visible');
				$spaceballs.debug(" - OK ('"+query+"')");
			}
		};
		
		if (headers) {
			for (h in headers) {
				console.log(h);
				//xmlhttp.setRequestHeader(header, value)
			}
		}
		
		xmlhttp.open('GET', $spaceballs.endpoint + query);
		xmlhttp.send();
		
		
	}
}


window.addEventListener('load', function (b) {
  $spaceballs.init()
});

